﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation.Examples;


public class Orbit : MonoBehaviour
{
    // Orbit around target when it is close enough
    private GeneratePathExample path;

    private bool orbit = false;
    private float speed = 100;
    private Quaternion randomAxis;
    public GameObject target;
    void Start()
    {
        path = gameObject.transform.parent.GetComponent<GeneratePathExample>();
        randomAxis = Random.rotation;
    }

    void Update()
    {
        if (orbit)
        {
            transform.RotateAround(target.transform.position, randomAxis.eulerAngles, speed * Time.deltaTime);
            gameObject.transform.SetParent(target.transform);
        }

        else if (Vector3.Distance(transform.position, path.waypoints[2].position) < 1f)
        {
            orbit = true;
            gameObject.GetComponent<PathFollower>().enabled = false;
        }
    }
}
