﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;


public class LightToggle : MonoBehaviour
{
    // Toggles an the Interactable's Light child when an Interactor with the right layermask is selected. 
    private XRBaseInteractor interactor;
    private XRBaseInteractable interactable;
    public LayerMask LightLayer;
    public LayerMask XRControllerLayer;
    private GameObject lightObject;
    public bool enableMaterialShift;
    public GameObject materialModel;
    public Material lightedMaterial;
    private Material defaultMaterial;


    void Start()
    {
        interactable = gameObject.GetComponent<XRBaseInteractable>();
        lightObject = gameObject.transform.Find("Light").gameObject;
        if (enableMaterialShift == true)
        {
            defaultMaterial = materialModel.GetComponent<Renderer>().material;
        }

    }
    public void ActivateLight()
    {
        interactor = interactable.selectingInteractor;

        // If the object is selected by something other than an XR Controller, activate the light
        if (!(interactor.interactionLayerMask == (interactor.interactionLayerMask | (1 << XRControllerLayer.value))))

        {
            lightObject.SetActive(true);
            if (enableMaterialShift == true)
            {
                materialModel.GetComponent<Renderer>().materials[1] = lightedMaterial;
                Debug.Log("Lighted");
            }

        }
    }

    public void DeactivateLight()
    {
        lightObject.SetActive(false);
        if (enableMaterialShift == true)
        {
            materialModel.GetComponent<Renderer>().material = defaultMaterial;
        }

    }
}
