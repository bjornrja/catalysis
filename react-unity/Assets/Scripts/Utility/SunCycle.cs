﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SunCycle : MonoBehaviour
{
    // Rotate the sun around the scene
    private Quaternion startRot;
    private Quaternion spinningRot;
    private ScenarioManager scenarioManager;
    void Start()
    {
        startRot = gameObject.transform.localRotation;
        spinningRot = startRot;
        scenarioManager = GameObject.Find("ScenarioManager").GetComponent<ScenarioManager>();
    }


    void Update()
    {
        if (scenarioManager.transitionDuration > scenarioManager.timeElapsed)
        {
            spinningRot = startRot * Quaternion.Euler(scenarioManager.rotations * 360 * scenarioManager.DistancePercent.Evaluate(scenarioManager.timeElapsed / scenarioManager.transitionDuration), 0, 0);
            gameObject.transform.localRotation = spinningRot;
        }
    }
}
