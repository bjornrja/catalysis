﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ManipulateMultipleInteractables : MonoBehaviour
{
    // Used to freeze multiple interactables. 

    public bool enableFreezeInFuture;
    [SerializeField] private List<GameObject> interactables;

    void Start()
    {
        GetAndSetInteractables();
    }

    public void GetAndSetInteractables()
    {
        // Collects all interactables with the correct tags due for freezing. 

        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Interactable"))
        {
            interactables.Add(obj);
        }
        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Interactable, secondary"))
        {
            interactables.Add(obj);
        }
    }



    public void FreezeInteractables(bool toggle)
    {
        // Toggles freezing according to input. 

        if (enableFreezeInFuture)
        {

            foreach (GameObject obj in interactables)
            {
                if (obj != null)
                {

                    Collider collider = obj.GetComponent<Collider>();
                    var rb = obj.GetComponent<Rigidbody>();

                    collider.enabled = !toggle;

                    if (toggle == true)
                    {
                        rb.constraints = RigidbodyConstraints.FreezeAll;
                    }

                    else if (toggle == false)
                    {
                        rb.constraints = RigidbodyConstraints.None;
                    }
                }
            }
        }
    }
}