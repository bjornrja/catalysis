using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.InputSystem;


public class Restarter : MonoBehaviour
{
    public InputAction leftPressed;
    public InputAction rightPressed;
    private int counter;
    private bool receltlyRestarted;

    void Start()
    {
        counter = 0;
        receltlyRestarted = true;
        StartCoroutine(CountDown());
    }
    void OnEnable()
    {
        leftPressed.Enable();
        rightPressed.Enable();
        leftPressed.performed += HandleLeftPress;
        leftPressed.canceled += HandleLeftCanceled;
        rightPressed.performed += HandleRightPress;
        rightPressed.canceled += HandleRightCanceled;
    }
    void OnDisable()
    {
        leftPressed.Disable();
        rightPressed.Disable();
        leftPressed.performed -= HandleLeftPress;
        leftPressed.canceled -= HandleLeftCanceled;
        rightPressed.performed -= HandleRightPress;
        rightPressed.canceled -= HandleRightCanceled;
    }

    private void HandleLeftPress(InputAction.CallbackContext context)
    {
        JoySticksPressed(1);
        Debug.Log("Left pressed");
    }
    private void HandleLeftCanceled(InputAction.CallbackContext context)
    {
        JoySticksPressed(-1);
        Debug.Log("Left canceled");
    }
    private void HandleRightPress(InputAction.CallbackContext context)
    {
        JoySticksPressed(1);
        Debug.Log("Right pressed");
    }
    private void HandleRightCanceled(InputAction.CallbackContext context)
    {
        JoySticksPressed(-1);
        Debug.Log("Right canceled");
    }

    private void JoySticksPressed(int input)
    {
        counter += input;
        if (counter < 0)
        {
            counter = 0;
        }
        if (counter >= 2 && receltlyRestarted == false)
        {
            Debug.Log("RESTARTGING");
            //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            SceneManager.LoadScene(0);
            receltlyRestarted = true;
            OnDisable();
            StartCoroutine(CountDown());
        }
    }


    private IEnumerator CountDown()
    {
        yield return new WaitForSeconds(5);
        Debug.Log("Restart ready");
        receltlyRestarted = false;

    }

}

