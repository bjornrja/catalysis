﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ScenarioManager : MonoBehaviour
{
    // Manages the transition to and from the future

    [Header("General settings")]
    public float transitionDuration;
    public GameObject Activator;
    public CarbonController carbonController;
    private ManipulateMultipleInteractables mmInteractables;
    public AnimationCurve DistancePercent;
    [HideInInspector] public bool inThePresent;
    [HideInInspector] public bool isTransitioning;
    [HideInInspector] public int currentYear;

    [Header("Water settings")]
    public float minWaterHeightIncrease;
    public float maxWaterHeightIncrease;

    private float trajectedWaterHeight;

    private float startHeight;
    [HideInInspector] public float seaLevelRisen;
    public GameObject Water;

    [Header("Sun settings")]
    public int sunRotations;
    [HideInInspector] public int rotations;
    [HideInInspector] public float targetHeight, time, yearsToTravel, timeElapsed;

    [Header("Temperature settings")]
    public float minTempIncrease;
    public float maxTempIncrease;
    private float trajectedTemperature, tempDiff;
    [HideInInspector] public float currentTemperature;
    [HideInInspector] public UnityEvent onTimeTravelForward;
    [HideInInspector] public UnityEvent onTimeTravelBackward;
    private bool textIsActive = false;


    void Start()
    {
        inThePresent = true;

        startHeight = Water.transform.position.y;

        trajectedWaterHeight = maxWaterHeightIncrease;
        trajectedTemperature = maxTempIncrease;

        mmInteractables = gameObject.GetComponent<ManipulateMultipleInteractables>();

        currentYear = 2021;

        onTimeTravelForward = new UnityEvent();
        onTimeTravelBackward = new UnityEvent();
    }

    public void reduceCO2(int molecules)
    {
        carbonController.RemoveMolecules(molecules);

        float percentCarbonRemaining = ((float)molecules / 15);

        var waterReduction = (maxWaterHeightIncrease - minWaterHeightIncrease) * percentCarbonRemaining;
        trajectedWaterHeight -= waterReduction;

        var tempReduction = (maxTempIncrease - minTempIncrease) * percentCarbonRemaining;
        trajectedTemperature -= tempReduction;
    }


    void Forward(float seconds)
    {
        // Transition forward in time
        time = seconds;
        timeElapsed = 0;
        inThePresent = false;

        textIsActive = true;

        onTimeTravelForward.Invoke();

        mmInteractables.FreezeInteractables(true);

        targetHeight = trajectedWaterHeight;
        rotations = sunRotations;
        yearsToTravel = 29;
        tempDiff = trajectedTemperature;

        isTransitioning = true;
    }

    void Backward(float seconds)
    {
        // Transition backward in time
        time = seconds;
        timeElapsed = 0;
        inThePresent = true;

        onTimeTravelBackward.Invoke();

        mmInteractables.FreezeInteractables(false);

        targetHeight = -targetHeight;
        rotations = -sunRotations;
        yearsToTravel = -29;
        tempDiff = -trajectedTemperature;

        isTransitioning = true;
    }


    void Update()
    {
        if (isTransitioning)
        {
            timeElapsed += Time.deltaTime;

            // Multiply with 100 to convert from m to cm. 
            // Used by VariableInfo.cs
            seaLevelRisen = (Water.transform.position.y - startHeight) * 100;

            var curveValue = DistancePercent.Evaluate(timeElapsed / transitionDuration);

            if (!inThePresent)
            {
                currentTemperature = tempDiff * curveValue;
                currentYear = (int)(2021 + yearsToTravel * curveValue + 0.5f);
            }

            else if (inThePresent)
            {
                currentTemperature = trajectedTemperature + tempDiff * curveValue;
                currentYear = (int)(2050 + yearsToTravel * curveValue + 0.5f);
            }
        }

        // The Carbon Sphere GameObject activates the Activator with a selection event
        if (Activator.activeInHierarchy && isTransitioning == false)
        {
            Activator.SetActive(false);

            if (inThePresent == true)
            {
                Forward(transitionDuration);
            }

            else if (inThePresent == false)
            {
                Backward(transitionDuration);
            }
        }


        if (time <= timeElapsed)
        {
            isTransitioning = false;

            // Deactivate the textual parameters when in the present
            if (textIsActive && inThePresent)
            {
                gameObject.GetComponent<FadeText>().FadeOut();
                textIsActive = false;
            }
        }
    }
}

