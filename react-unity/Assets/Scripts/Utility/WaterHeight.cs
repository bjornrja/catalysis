﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterHeight : MonoBehaviour
{
    // Move the water height up and down
    [HideInInspector] public Vector3 startPosition;
    private Vector3 targetPosition;
    private ScenarioManager scenarioManager;

    void Start()
    {
        startPosition = gameObject.transform.position;
        targetPosition = startPosition;
        scenarioManager = GameObject.Find("ScenarioManager").GetComponent<ScenarioManager>();

    }

    void Update()
    {
        if (scenarioManager.transitionDuration > scenarioManager.timeElapsed)
        {
            if (scenarioManager.targetHeight != 0)
            {
                gameObject.transform.position = new Vector3(gameObject.transform.position.x, startPosition.y + scenarioManager.targetHeight * scenarioManager.DistancePercent.Evaluate(scenarioManager.timeElapsed / scenarioManager.transitionDuration), gameObject.transform.position.z);
            }
        }

        else
        {
            startPosition = gameObject.transform.position;
        }
    }
}

