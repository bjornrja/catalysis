﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;


public class CarbonController : MonoBehaviour
{
    // Instansiates, tracks and removes Molecule GameObjects. 
    [HideInInspector] public int carbonCount;
    public int startingCarbonCount;
    [HideInInspector] public UnityEvent onMoleculesChanged;
    [HideInInspector] public List<GameObject> molecules;

    private Transform moleculeTransform;

    void Start()
    {
        carbonCount = startingCarbonCount;

        moleculeTransform = gameObject.transform.Find("Molecule");

        for (int i = 0; i < carbonCount; i++)
        {
            GameObject instance = Instantiate(moleculeTransform.gameObject);
            instance.SetActive(true);

            instance.transform.parent = gameObject.transform;
            molecules.Add(instance);
        }


        onMoleculesChanged = new UnityEvent();
        onMoleculesChanged.AddListener(UpdateCarbonCount);

    }

    public void UpdateCarbonCount(){
        carbonCount = molecules.Count;
    }

    public void RemoveMolecules(int number)
    {
        {
            for (int i = 0; i < number; i++)
            {
                var moleculeTransform = molecules[molecules.Count - 1];
                molecules.RemoveAt(molecules.Count - 1);

                // Starts the molecule removal animation
                moleculeTransform.gameObject.GetComponent<MoleculeBehaviour>().FreeMolecule();
            }

            onMoleculesChanged.Invoke();

        }
    }
}
