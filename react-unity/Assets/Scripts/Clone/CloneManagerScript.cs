﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloneManagerScript : MonoBehaviour
{
    // Spawns multiple clones in a pattern. 
    public GameObject clone, origo, spawnPoint;
    private GameObject cloneFolder;
    public List<GameObject> objectList;

    void Start()
    {
        PopulateArc(50);
    }

    public void PopulateArc(int length)
    {
        // Instansiate copies of the clone GameObject in a arc with increasing distances. 
        // The origo GameObject is the axis that the spawnPoint GameObject rotates around. 

        cloneFolder = clone.transform.parent.gameObject;
        var normalPos = spawnPoint.transform.localPosition;

        for (float i = 1; i <= length; i += 1)
        {
            // Move the spawner to the left and instansiate
            origo.transform.Rotate(0, i * 2.5f, 0, Space.Self);
            GameObject leftInstance = Instantiate(clone.gameObject);
            leftInstance.transform.position = spawnPoint.transform.position;
            leftInstance.transform.rotation = new Quaternion(0f, 180, 0f, 0f);
            leftInstance.SetActive(true);
            leftInstance.transform.SetParent(cloneFolder.transform);
            origo.transform.Rotate(0, -i * 2.5f, 0, Space.Self);

            objectList.Add(leftInstance.transform.GetChild(3).gameObject);


            // Move the spawner to the right and instansiate
            origo.transform.Rotate(0, -i * 2.5f, 0, Space.Self);
            GameObject rightInstance = Instantiate(clone.gameObject);
            rightInstance.transform.position = spawnPoint.transform.position;
            rightInstance.transform.rotation = new Quaternion(0f, 180, 0f, 0f);
            rightInstance.SetActive(true);
            rightInstance.transform.SetParent(cloneFolder.transform);
            origo.transform.Rotate(0, i * 2.5f, 0, Space.Self);

            objectList.Add(rightInstance.transform.GetChild(3).gameObject);

            // After ten iterations, move the spawnPoint incrementally further away from the origo
            if (i > 10)
            {
                spawnPoint.transform.localPosition *= Mathf.Pow(1.002f, i - 10);
            }
        }
    }

    public void PopulateLong(int length)
    {

        cloneFolder = clone.transform.parent.gameObject;

        for (float i = 0; i <= length; i += 1)
        {
            GameObject leftInstance = Instantiate(clone.gameObject);
            leftInstance.transform.position = new Vector3(i + 1, 0, 0);
            leftInstance.SetActive(true);
            leftInstance.transform.SetParent(cloneFolder.transform);

            GameObject rightInstance = Instantiate(clone.gameObject);
            rightInstance.transform.position = new Vector3(-i - 1, 0, 0);
            rightInstance.SetActive(true);
            rightInstance.transform.SetParent(cloneFolder.transform);
        }
        cloneFolder.transform.rotation = Quaternion.Euler(0, 180, 0);




    }
    public void Populate(int length)
    {
        cloneFolder = clone.transform.parent.gameObject;


        for (int i = 0; i < length + 1; i++)
        {
            var x = i * .25f;

            GameObject cloneRow = new GameObject();
            cloneRow.name = "Clonerow " + i;
            cloneRow.transform.SetParent(cloneFolder.transform);


            for (float j = 0; j <= i; j += 1)
            {
                GameObject leftInstance = Instantiate(clone.gameObject);
                leftInstance.transform.position = new Vector3(i + 1, 0, -j - x);
                leftInstance.SetActive(true);
                leftInstance.transform.parent = cloneRow.transform;

                GameObject rightInstance = Instantiate(clone.gameObject);
                rightInstance.transform.position = new Vector3(-i - 1, 0, -j - x);
                rightInstance.SetActive(true);
                rightInstance.transform.parent = cloneRow.transform;
            }
        }

        cloneFolder.transform.rotation = Quaternion.Euler(0, 180, 0);
    }

}
