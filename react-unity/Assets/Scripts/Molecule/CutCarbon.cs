﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutCarbon : MonoBehaviour
{
    // Defines behaviour of molecules that have been removed from the Carbon Controller. 
    public List<GameObject> cutMolecules, movingMolecules;
    public float spacing = 0.02f;

    public void AddMolecule(GameObject molecule)
    {
        cutMolecules.Add(molecule);
        molecule.transform.SetParent(transform);

        if (cutMolecules.Count == 1)
        {
            ReadyNextMolecule();
        }

        UpdatePositions();
    }

    public void ReadyNextMolecule()
    {
        // Make the first molecule big and interactable. 

        GameObject oldMolecule = cutMolecules[0];

        Transform interactableMoleculeTransform = gameObject.transform.Find("Molecule, interactable");
        GameObject newMolecule = Instantiate(interactableMoleculeTransform.gameObject);
        newMolecule.SetActive(true);
        cutMolecules[0] = newMolecule;
        newMolecule.transform.SetParent(newMolecule.GetComponent<MoleculeBehaviour>().carbonCutAnchor.transform);
        newMolecule.GetComponent<MoleculeBehaviour>().nextMolecule = true;
        newMolecule.transform.localScale *= 2;
        Destroy(oldMolecule);
    }

    public void UpdatePositions()
    {
        // Move the molecules to their positions
        for (int i = 0; i < cutMolecules.Count; i++)
        {
            cutMolecules[i].transform.localPosition = new Vector3(0f, 0f, i * (cutMolecules.Count * spacing));
        }
    }
    public void DragAlongMolecules()
    {
        // Make the molecules follow each other
        if (cutMolecules.Count > 1)
        {
            for (int i = 1; i < cutMolecules.Count; i++)
            {
                cutMolecules[i].GetComponent<MoleculeBehaviour>().FollowNext(cutMolecules[i - 1]);
            }

        }

        movingMolecules = new List<GameObject>(cutMolecules);
        cutMolecules.Clear();
    }

    public void PopMovingMolecules()
    {
        // Destroys the molecules
        for (int i = 0; i < movingMolecules.Count; i++)
        {
            movingMolecules[i].GetComponent<MoleculeBehaviour>().following = false;
            StartCoroutine(DelayedDestroy(movingMolecules[i], i));
        }
    }

    IEnumerator DelayedDestroy(GameObject molecule, float index)
    {
        yield return new WaitForSeconds(index / 10);

        // Increase the pitch of the sound for every molecule
        float newPitch = 1 + index / 5f;
        molecule.GetComponent<AudioSource>().pitch = newPitch;
        AudioSource.PlayClipAtPoint(molecule.GetComponent<MoleculeBehaviour>().popSound, gameObject.transform.position);

        yield return new WaitForSeconds(.2f);
        Destroy(molecule);
    }





}

