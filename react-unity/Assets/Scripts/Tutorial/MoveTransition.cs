﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTransition : MonoBehaviour
{
    public AnimationCurve travelGraph;
    public float moveDuration;
    public Vector3 positionChange;
    private float timeElapsed;
    private Vector3 startPosition;
    private bool move = false;

    void Start()
    {
        startPosition = gameObject.transform.position;
    }

    public void Move()
    {
        move = true;
    }


    void Update()
    {
        if (move == true && travelGraph.Evaluate(timeElapsed / moveDuration) < 0.95)
        {
            timeElapsed += Time.deltaTime;
            transform.position = startPosition + (positionChange * travelGraph.Evaluate(timeElapsed / moveDuration));
        }

        else
        {
            move = false;
        }
    }



}
