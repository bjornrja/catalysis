﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeText : MonoBehaviour
{
    // Fade textual parameters
    public Material material;
    public float startingAlpha;
    public float fadeFrequency = 0.05f;
    public float fadeDuration = 1.0f;

    private ScenarioManager scenarioManager;
    [HideInInspector] public bool isActive;

    void Start()
    {
        Color color = material.color;
        color.a = startingAlpha;
        material.color = color;

        scenarioManager = gameObject.GetComponent<ScenarioManager>();
    }

    public void FadeIn()
    {
        // Fade in information text if user goes to future
        if (scenarioManager.inThePresent && !scenarioManager.isTransitioning)
        {
            isActive = true;
            StartCoroutine(FadeCoroutine(fadeFrequency, fadeDuration, true));
        }
    }
    public void FadeOut()
    {
        StartCoroutine(FadeCoroutine(fadeFrequency, fadeDuration, false));
    }

    private IEnumerator FadeCoroutine(float frequency, float duration, bool appear)
    {
        Color color = material.color;

        if (!appear)
        {
            for (float newAlpha = 1f; newAlpha >= 0f; newAlpha -= frequency / duration)
            {
                color.a = newAlpha;
                material.color = color;
                yield return new WaitForSeconds(frequency);
            }
            isActive = false;

        }

        else
        {
            for (float newAlpha = 0f; newAlpha <= 1f; newAlpha += frequency / duration)
            {
                color.a = newAlpha;
                material.color = color;
                yield return new WaitForSeconds(frequency);
            }

        }
        yield return "Finished";
    }

    public IEnumerator DelayedFadeOut(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        FadeOut();
    }
}
