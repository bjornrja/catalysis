﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoleculeCounter : MonoBehaviour
{
    // Displays the molecule count
    private Text progressCounter;
    private CarbonController carbonController;
    void Start()
    {
        carbonController = GameObject.Find("Carbon Controller").GetComponent<CarbonController>();

        progressCounter = GameObject.Find("Molecule Counter").GetComponent<UnityEngine.UI.Text>();


        StartCoroutine(DelayedCount(.1f));

        //UpdateCounter();

        // Quickfix
        progressCounter.text = "15/15";
    }

    IEnumerator DelayedCount(float delay){
        yield return new WaitForSeconds(delay);
        carbonController.onMoleculesChanged.AddListener(UpdateCounter);
    }

    public void UpdateCounter()
    {
        progressCounter.text = carbonController.molecules.Count.ToString() + "/15";
    }

}


