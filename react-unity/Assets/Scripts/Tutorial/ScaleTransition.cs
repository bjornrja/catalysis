﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleTransition : MonoBehaviour
{
    // Upscale and downscale GameObject
    private Vector3 defaultScale;
    public AnimationCurve scaleGraph;
    private bool upscaleObject = false, downscaleObject = false;
    private float timeElapsed = 0;
    private float duration = 0;


    void Start()
    {
        defaultScale = gameObject.transform.localScale;
        Upscale(1);
    }

    public void Upscale(float time)
    {
        gameObject.transform.localScale *= 0.01f;
        duration = time;
        upscaleObject = true;
    }
    public void Downscale(float time)
    {
        duration = time;
        downscaleObject = true;
        defaultScale = gameObject.transform.localScale;

    }


    void Update()
    {
        if (upscaleObject)
        {
            float percent = scaleGraph.Evaluate(timeElapsed + 0.01f / duration);
            if (percent > 0.95)
            {
                upscaleObject = false;
                timeElapsed = 0;
            }
            else
            {
                timeElapsed += Time.deltaTime;

                gameObject.transform.localScale = percent * defaultScale;

            }
        }


        else if (downscaleObject)
        {
            float percent = scaleGraph.Evaluate(timeElapsed + 0.01f / duration);

            if (percent > 0.95)
            {
                downscaleObject = false;
                timeElapsed = 0;

                gameObject.SetActive(false);

            }
            else
            {
                timeElapsed += Time.deltaTime;

                gameObject.transform.localScale = defaultScale - percent * defaultScale;
            }
        }
    }
}
