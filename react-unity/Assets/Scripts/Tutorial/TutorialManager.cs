﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TutorialManager : MonoBehaviour
{
    // Present different tasks and object

    private int index;
    public GameObject button, redCube, orangeSocket, orangeCube, chalkboard, house, carbonSphere;
    public GameObject one, two, three;
    public GameObject models;
    public GameObject moleculeManager;
    public GameObject targetA, targetB, earth;
    public GameObject carbonCube, carbonSocket, clones;

    public GameObject table;
    public AudioClip clip1, clip2, clip3, clip4, clip5, clip6, clip7, clip8, clip9, clip10, clip11, clip12, clip13, clip14, clip15, clip16, clip17;
    [HideInInspector] public List<GameObject> fadeInObjects, fadeOutObjects;
    public GameObject leftControllerModel, rightControllerModel, leftTeleportController, rightTeleportController, leftHand, rightHand;
    private AudioSource audioSource;
    public Material transparentHandMaterial;
    private CloneManagerScript cloneManagerScript;
    private ScenarioManager scenarioManager;

    void Start()
    {
        List<GameObject> fadeInObjects = new List<GameObject>();

        button.SetActive(false);
        carbonSphere.SetActive(false);

        audioSource = gameObject.GetComponent<AudioSource>();
        cloneManagerScript = GameObject.FindObjectOfType<CloneManagerScript>();
        scenarioManager = GameObject.Find("ScenarioManager").GetComponent<ScenarioManager>();

        // Start the tutorial
        ActivateAndFade(button);
        StartCoroutine(DelayedPlay(1.5f, clip1));
        StartCoroutine(DelayedReady(1.5f + clip1.length));
    }

    public void NextTask()
    {
        // Iterate though the transitions

        // Fade in first cube
        if (index == 0)
        {
            ActivateAndFade(redCube);
            audioSource.clip = clip2;
            audioSource.Play();
        }

        // Fade in another cube and a socket
        if (index == 1)
        {
            redCube.SetActive(false);
            ActivateAndFade(orangeSocket);
            ActivateAndFade(orangeCube);
            audioSource.clip = clip3;
            audioSource.Play();
            index = 2;
        }

        // Activate carbon sphere
        if (index == 3)
        {
            orangeSocket.SetActive(false);
            orangeCube.SetActive(false);

            carbonSphere.SetActive(true);
            audioSource.clip = clip4;
            audioSource.Play();
        }

        // Fade in "frozen" cubes after using the carbon cube
        if (index == 4)
        {
            ActivateAndFade(one);
            ActivateAndFade(two);
            ActivateAndFade(three);
        }

        // See CarbonSphereTutorial.cs for Clip 5 and 6. 


        // Fade out cubes, table and button
        if (index == 5)
        {
            FadeAndDeactivate(one);
            FadeAndDeactivate(two);
            FadeAndDeactivate(three);
            one.GetComponent<Rigidbody>().useGravity = false;
            two.GetComponent<Rigidbody>().useGravity = false;
            three.GetComponent<Rigidbody>().useGravity = false;

            FadeAndDeactivate(table);
            FadeAndDeactivate(button);
            StartCoroutine(DelayedNext(3));

        }

        // Face in car, plane, cow and pig
        if (index == 6)
        {
            ActivateAndFade(models);
            StartCoroutine(DelayedPlay(2, clip7));
            StartCoroutine(DelayedNext(4));
        }

        // Start "carbon emission"
        if (index == 7)
        {
            moleculeManager.SetActive(true);
        }

        // Remove the emission sources
        if (index == 8)
        {
            // NextTask() called from MoleculeManager
            models.GetComponent<MoveTransition>().Move();
            StartCoroutine(DelayedPlay(2, clip8));
            StartCoroutine(DelayedNext(clip8.length / 2 + 1));
        }

        // Fade in earth
        if (index == 9)
        {
            ActivateAndFade(earth);
            StartCoroutine(DelayedNext(2 + clip8.length / 2));
        }

        // Fade out earth
        if (index == 10)
        {
            FadeAndDeactivate(earth);
            StartCoroutine(DelayedNext(2));
        }


        // Split molecules in half
        if (index == 11)
        {

            targetA.GetComponent<MoveTransition>().Move();
            targetB.GetComponent<MoveTransition>().Move();

            audioSource.clip = clip9;
            audioSource.Play();

            StartCoroutine(DelayedNext(clip9.length));
        }

        // Remove right half of molecules
        if (index == 12)
        {
            models.SetActive(false);
            targetB.GetComponent<ScaleTransition>().Downscale(1);

            StartCoroutine(DelayedPlay(1, clip10));

            StartCoroutine(DelayedNext(1 + clip10.length));
        }

        // Place left molecules on hand
        if (index == 13)
        {
            foreach (Transform child in targetA.transform)
            {
                child.gameObject.GetComponent<Orbit>().enabled = false;
                child.gameObject.GetComponent<MoveToSphere>().enabled = true;
            }

            carbonSphere.transform.GetComponentInChildren<CarbonController>().enabled = true;

            StartCoroutine(DelayedNext(4));
        }


        // Show the carbon emission reduction animation
        if (index == 14)
        {
            StartCoroutine(DelayedPlay(0, clip11));
            StartCoroutine(LoopCarbonCuts(4, 1));
            StartCoroutine(DelayedNext(1 + clip11.length + 3));
        }

        // Fade in clones
        if (index == 15)
        {
            audioSource.clip = clip12;
            audioSource.Play();

            ActivateAndFade(clones);
            Destroy(GameObject.Find("Clone"));

            foreach (GameObject box in cloneManagerScript.objectList)
            {
                box.SetActive(false);
            }

            StartCoroutine(DelayedNext(clip12.length));
        }

        // Fade in table and buttons
        if (index == 16)
        {
            ActivateAndFade(table);
            ActivateAndFade(button);
            button.GetComponent<Button>().SetReady(false);

            ActivateAndFade(carbonSocket);
            ActivateAndFade(carbonCube);

            StartCoroutine(DelayedPlay(1.5f, clip13));

            foreach (GameObject box in cloneManagerScript.objectList)
            {
                ActivateAndFade(box);
            }

        }

        // Fade in house
        if (index == 17)
        {
            FadeAndDeactivate(clones);
            audioSource.clip = null;


            FadeAndDeactivate(carbonCube);
            FadeAndDeactivate(carbonSocket);

            house.SetActive(true);

            StartCoroutine(DelayedPlay(2, clip14));
            StartCoroutine(DelayedNext(clip14.length + 2));
        }

        // Fade out house
        if (index == 18)
        {
            house.GetComponent<ScaleTransition>().Downscale(1);
            StartCoroutine(DelayedNext(1));

        }

        // Fade in chalkboard
        if (index == 19)
        {
            chalkboard.SetActive(true);

            StartCoroutine(DelayedPlay(0.5f, clip15));
            StartCoroutine(DelayedNext(clip15.length + 2));
        }


        // Make controllers visible and enable teleportation
        if (index == 20)
        {
            chalkboard.GetComponent<ScaleTransition>().Downscale(1);
            leftControllerModel.SetActive(true);
            rightControllerModel.SetActive(true);
            leftTeleportController.SetActive(true);
            rightTeleportController.SetActive(true);
            leftHand.GetComponent<Renderer>().material = transparentHandMaterial;
            rightHand.GetComponent<Renderer>().material = transparentHandMaterial;

            StartCoroutine(DelayedPlay(2, clip16));
            StartCoroutine(DelayedNext(7 + clip16.length));
        }


        if (index == 21)
        {
            StartCoroutine(DelayedPlay(0, clip17));
        }


        index += 1;
    }


    public void ActivateAndFade(GameObject gObject)
    {
        // Activate and fade in the GameObject and it's children
        fadeInObjects.Add(gObject);

        GetChildren(gObject, fadeInObjects); //recursive

        foreach (GameObject obj in fadeInObjects)
        {
            obj.SetActive(true);
            if (obj.GetComponent<Transition>() && obj.GetComponent<Renderer>())
            {
                obj.GetComponent<Transition>().FadeInSingle();
            }
        }

        fadeInObjects.Clear();

    }

    public void FadeAndDeactivate(GameObject gObject)
    {
        // Fade out and deactivate the GameObject and it's children

        fadeOutObjects.Add(gObject);
        GetChildren(gObject, fadeOutObjects);
        foreach (GameObject obj in fadeOutObjects)
        {
            if (obj.GetComponent<Transition>() && obj.GetComponent<Renderer>())
            {
                obj.GetComponent<Transition>().FadeOutSingle();
            }

            else if (obj.GetComponent<Transition>())
            {
                obj.GetComponent<Transition>().DelayedDeactivate();

            }

        }
        fadeOutObjects.Clear();

    }

    private void GetChildren(GameObject obj, List<GameObject> list)
    {
        // Recursivly get all children of a GameObject
        if (null == obj)
        {
            return;
        }
        foreach (Transform child in obj.transform)
        {
            if (null == child)
                continue;

            list.Add(child.gameObject);
            GetChildren(child.gameObject, list);
        }
    }

    IEnumerator DelayedPlay(float seconds, AudioClip audioClip)
    {
        yield return new WaitForSeconds(seconds);

        audioSource.clip = audioClip;
        audioSource.Play();
    }

    IEnumerator DelayedReady(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        button.GetComponent<Button>().SetReady(true);
    }

    public IEnumerator DelayedNext(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        NextTask();
    }

    IEnumerator LoopCarbonCuts(int times, float delay)
    {
        yield return new WaitForSeconds(2);

        for (int clip8 = 0; clip8 < times; clip8++)
        {
            scenarioManager.reduceCO2(1);
            yield return new WaitForSeconds(delay);
        }
    }
}
