﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChalkboardManager : MonoBehaviour
{
    // Present the task progress
    public Text infoText;
    private float totalSolarCount, totalLightCount, totalWindowCount;
    private float currentSolarCount, currentLightCount, currentWindowCount;
    private float currentSolarPercent, currentLightPercent, currentWindowPercent;

    void Start()
    {
        totalSolarCount = 1;
        totalLightCount = 4;
        totalWindowCount = 6;

        currentLightCount = 0;
        currentSolarCount = 0;
        currentWindowCount = 0;

        currentWindowPercent = 0;
        currentLightPercent = 0;
        currentSolarPercent = 0;

        UpdateChalkBoard();

    }

    public void OnTaskPerformed(string layer)
    {
        if (layer == "Light")
        {
            currentLightCount += 1;
            currentLightPercent = (currentLightCount / totalLightCount) * 100;
        }
        else if (layer == "Solar")
        {
            currentSolarCount += 1;
            currentSolarPercent = (currentSolarCount / totalSolarCount) * 100;
        }
        else if (layer == "Window")
        {
            currentWindowCount += 1;
            currentWindowPercent = (currentWindowCount / totalWindowCount) * 100;
        }

        UpdateChalkBoard();
    }


    void UpdateChalkBoard()
    {
        infoText.text = currentLightPercent.ToString("N2") + "%\n" + currentWindowPercent.ToString("N2") + "%\n" + currentSolarPercent.ToString("N2") + "%";
    }
}
