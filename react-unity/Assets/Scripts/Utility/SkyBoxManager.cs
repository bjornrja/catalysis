﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyBoxManager : MonoBehaviour
{
    // Change the skybox according to carbon emissions reduced

    public Material goodSkyBox, badSkyBox, normalSkyBox, currentSkyBox;
    private float goodConv, badConv, normalConv;
    private float goodExpo, badExpo, normalExpo;
    private ScenarioManager scenarioManager;
    private CarbonController carbonController;
    public GameObject sun;
    private Light sunLight;
    public float maxSunStrength, normalSunStrength, minSunStrength;
    void Start()
    {
        scenarioManager = GameObject.Find("ScenarioManager").GetComponent<ScenarioManager>();
        StartCoroutine(AddDelayedListener(.1f));

        carbonController = GameObject.Find("Carbon Controller").GetComponent<CarbonController>();

        // Get values for sun size convergence
        goodConv = goodSkyBox.GetFloat("_SunSizeConvergence");
        badConv = badSkyBox.GetFloat("_SunSizeConvergence");
        normalConv = normalSkyBox.GetFloat("_SunSizeConvergence");

        // Get values for exposure
        goodExpo = goodSkyBox.GetFloat("_Exposure");
        badExpo = badSkyBox.GetFloat("_Exposure");
        normalExpo = normalSkyBox.GetFloat("_Exposure");


        // Set starting skybox
        currentSkyBox.SetFloat("_SunSizeConvergence", normalConv);
        currentSkyBox.SetFloat("_Exposure", normalExpo);
        sunLight = sun.GetComponent<Light>();
        sunLight.intensity = normalSunStrength;
    }

    void ResetSkybox()
    {
        float convChange = normalConv - currentSkyBox.GetFloat("_SunSizeConvergence");
        float expoChange = normalExpo - currentSkyBox.GetFloat("_Exposure");
        float sunlightChange = normalSunStrength - sunLight.intensity;

        StartCoroutine(FadeSkybox(scenarioManager.transitionDuration, convChange, expoChange, sunlightChange));
    }

    void SetAndFadeSkybox()
    {
        var percentCarbonRemaining = (float)carbonController.carbonCount / (float)carbonController.startingCarbonCount;
        float convChange = 0, expoChange = 0, sunlightChange = 0;


        if (percentCarbonRemaining > .60f)
        {
            // Increase variables as percentCarbonRemaining increases above 60%
            convChange = (badConv - currentSkyBox.GetFloat("_SunSizeConvergence")) * (percentCarbonRemaining - 0.60f) * 2.50f;
            expoChange = (badExpo - currentSkyBox.GetFloat("_Exposure")) * (percentCarbonRemaining - 0.60f) * 2.50f;

            sunlightChange = (minSunStrength - normalSunStrength) * (percentCarbonRemaining - 0.60f) * 2.50f;
        }

        else if (percentCarbonRemaining < .40f)
        {
            // Decrease variables as percentCarbonRemaining decreases below 40%
            convChange = (goodConv - currentSkyBox.GetFloat("_SunSizeConvergence")) * (1 - (percentCarbonRemaining * 2.50f));
            expoChange = (goodExpo - currentSkyBox.GetFloat("_Exposure")) * (1 - (percentCarbonRemaining * 2.50f));

            sunlightChange = (maxSunStrength - normalSunStrength) * (1 - (percentCarbonRemaining * 2.50f));
        }

        StartCoroutine(FadeSkybox(scenarioManager.transitionDuration, convChange, expoChange, sunlightChange));
    }



    private IEnumerator FadeSkybox(float time, float convChange, float expoChange, float sunlightChange)
    {
        float updatesPerSecond = 0.01f;
        float convPrevious = currentSkyBox.GetFloat("_SunSizeConvergence");
        float expoPrevious = currentSkyBox.GetFloat("_Exposure");
        float sunPrevious = sunLight.intensity;

        for (float i = 0; i < time; i += Time.deltaTime)
        {
            currentSkyBox.SetFloat("_SunSizeConvergence", convPrevious + convChange * scenarioManager.timeElapsed / time);
            currentSkyBox.SetFloat("_Exposure", expoPrevious + expoChange * scenarioManager.timeElapsed / time);

            sunLight.intensity = sunPrevious + sunlightChange * (scenarioManager.timeElapsed / time);

            yield return new WaitForSeconds(updatesPerSecond);
        }
    }


    private IEnumerator AddDelayedListener(float waitSeconds)
    {
        yield return new WaitForSeconds(waitSeconds);
        scenarioManager.onTimeTravelForward.AddListener(SetAndFadeSkybox);
        scenarioManager.onTimeTravelBackward.AddListener(ResetSkybox);
    }

}
