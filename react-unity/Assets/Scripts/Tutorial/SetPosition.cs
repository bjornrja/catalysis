﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SetPosition : MonoBehaviour
{

    public GameObject anchor;
    void Update()
    {
        transform.position = anchor.transform.position;
        transform.rotation = anchor.transform.rotation;
    }
}
