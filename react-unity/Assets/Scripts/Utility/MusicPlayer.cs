﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MusicPlayer : MonoBehaviour
{
    // Loop audio, fade in and out audio clips
    public AudioSource pianoAudio;
    public AudioSource bassAudio;
    public AudioSource stringsAudio;
    public AudioSource ominousAudio;
    private ScenarioManager scenarioManager;
    private float bassChange, stringChange, ominousChange;
    private CarbonController carbonController;
    void Start()
    {
        scenarioManager = GameObject.Find("ScenarioManager").GetComponent<ScenarioManager>();
        StartCoroutine(AddDelayedListener(.1f));

        carbonController = GameObject.Find("Carbon Controller").GetComponent<CarbonController>();

    }

    void FadeInMusic()
    {
        var percentCarbonRemaining = (float)carbonController.carbonCount / (float)carbonController.startingCarbonCount;

        if (percentCarbonRemaining > 0.60f)
        {
            // Subtract 0.60f to shift volume change interval from [0.60f, 1.00f] to [0, 0.40f] 
            // Multiply with 2.5f to expand volume change interval from [0, 0.40f] to [0, 1.00f]
            ominousChange = (percentCarbonRemaining - 0.60f) * 2.5f;

        }
        else if (percentCarbonRemaining < .40f)
        {
            // Multiply with 2.5f to expand volume change interval from [0, 0.40f] to [0, 1.00f]
            stringChange = 1 - (percentCarbonRemaining * 2.5f);
            bassChange = 1 - (percentCarbonRemaining * 2.5f);
        }

        StartCoroutine(FadeAudio(scenarioManager.transitionDuration));

    }

    void FadeOutMusic()
    {
        bassChange = -1;
        stringChange = -1;
        ominousChange = -1;
        StartCoroutine(FadeAudio(scenarioManager.transitionDuration));
    }

    private IEnumerator FadeAudio(float time)
    {
        var updatesPerSecond = 0.01f;

        var previousBass = bassAudio.volume;
        var previosuString = stringsAudio.volume;
        var previousOminous = ominousAudio.volume;

        for (float i = 0; i < time; i += Time.deltaTime)
        {
            bassAudio.volume = previousBass + bassChange * scenarioManager.timeElapsed / time;
            stringsAudio.volume = previosuString + stringChange * scenarioManager.timeElapsed / time;
            ominousAudio.volume = previousOminous + ominousChange * scenarioManager.timeElapsed / time;
            yield return new WaitForSeconds(updatesPerSecond);
        }

    }

    private IEnumerator AddDelayedListener(float waitSeconds)
    {
        yield return new WaitForSeconds(waitSeconds);
        scenarioManager.onTimeTravelForward.AddListener(FadeInMusic);
        scenarioManager.onTimeTravelBackward.AddListener(FadeOutMusic);

    }
}
