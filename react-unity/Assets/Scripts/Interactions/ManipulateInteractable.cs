﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using System;


public class ManipulateInteractable : MonoBehaviour
{
    // Place on an Interactor to manipulate it's Interactable. 
    // Use Interactable Events (Select) in the "XR Grab Interactable" component to invoke StartHighlight or StopHighlight



    [Header("Lock settings (for gameobjects tagged 'Interactable)")]

    public bool enableLayerSwitching;
    private XRBaseInteractor interactor;
    private XRBaseInteractable interactable, previousInteractable;
    public LayerMask newLayers;


    [Header("Label settings(for LEDS)")]

    public LabelManager labelManager;
    private GameObject target;
    private XRBaseInteractable previouslySelectedInteractable;

    [Header("Rescale settings")]
    public bool enableRescaling;
    public Vector3 upscaleSize;
    public Vector3 downscaleSize;

    void Start()
    {
        interactor = gameObject.GetComponent<XRBaseInteractor>();
    }

    void Update()
    {
        if (interactor.selectTarget == true)
        {
            interactable = interactor.selectTarget;
            previousInteractable = interactable;
        }

        else
        {
            interactable = null;
        }
    }


    public void UpscaleInteractable()
    {
        if (enableRescaling == true)
        {
            interactable = interactor.selectTarget;
            interactable.gameObject.transform.localScale = upscaleSize;
        }
    }

    public void DownscaleInteractable()
    {
        if (enableRescaling == true)
        {
            // Previous Interactable is used because the function is commonly used after it is de-selected by an interactor.
            previousInteractable.gameObject.transform.localScale = downscaleSize;
        }
    }



    public void LEDLockInteractable()
    {
        SwitchLayers();
    }

    public void SwitchLayers()
    {
        // Switches the Interactable's interaction layer mask to a selected layer
        if (enableLayerSwitching == true)
        {
            interactable = interactor.selectTarget;

            // Ensures that object to be removed is not locked
            if (interactable.tag == "Interactable")
            {
                interactable.GetComponent<XRGrabInteractable>().interactionLayerMask = newLayers;
            }
        }
    }


    public void DisableLEDText()
    {
        target = interactor.selectTarget.gameObject;
        if (target.name == "LED")
        {
            labelManager.GetComponent<LabelManager>().HideLEDText();
        }
    }



}
