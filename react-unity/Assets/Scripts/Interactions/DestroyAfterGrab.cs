﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;


public class DestroyAfterGrab : MonoBehaviour
{
    // Destroys an object after it has been grabbed and released. 
    // Use Interactable Events (Select) in the "XR Grab Interactable" components to invoke ObjectSelected and AttemptDestroy

    private XRBaseInteractable interactable;
    public AudioClip audioClip;
    public float destroyAfterSeconds;
    private XRBaseInteractor interactor;
    public LayerMask XRControllerLayer;
    private bool heldByPlayer;

    void Start()
    {
        interactable = gameObject.GetComponent<XRBaseInteractable>();
        heldByPlayer = false;
    }

    public void ObjectSelected()
    {
        // Cache the selecting interactor
        if (interactable.isSelected == true)
        {
            interactor = interactable.selectingInteractor;
        }

        // If the object is grabbed by an XR Controller, remove all other layers from it's layermask
        // to stop it from being able to interact with other interactors.
        if (interactor.interactionLayerMask == (interactor.interactionLayerMask | (1 << XRControllerLayer.value)))
        {
            interactable.interactionLayerMask = XRControllerLayer;

            heldByPlayer = true;
        }
    }
    public void AttemptDestroy()
    {
        // If the previous selector was not an environmental interactor(AKA an XR controllers), destroy. 
        if (interactable.isSelected == false && heldByPlayer == true)
        {
            StartCoroutine(DelayedDestroy(destroyAfterSeconds));
        }
    }


    private IEnumerator DelayedDestroy(float delay)
    {
        yield return new WaitForSeconds(delay);
        AudioSource.PlayClipAtPoint(audioClip, gameObject.transform.position);
        Destroy(gameObject);

    }
}
