﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation.Examples;

public class CarbonSpawner : MonoBehaviour
{
    // Spawns Follower GameObjects
    private GameObject follower;

    private GeneratePathExample path;

    public float secondsBetweenSpawns = 1;
    private bool continueSpawning = true;
    private MoleculeManager moleculeManager;



    void Start()
    {
        path = gameObject.GetComponent<GeneratePathExample>();
        moleculeManager = GameObject.Find("MoleculeManager").GetComponent<MoleculeManager>();
        Spawn();
        StartCoroutine(SpawnLoop(secondsBetweenSpawns));


    }

    IEnumerator SpawnLoop(float secondsBetweenSpawns)
    {
        // Loops Spawn() until continueSpawning is set to false
        yield return new WaitForSeconds(secondsBetweenSpawns);
        Spawn();
        if (continueSpawning)
        {
            StartCoroutine(SpawnLoop(secondsBetweenSpawns));
        }
    }



    private void Spawn()
    {
        // Spawns a follower 
        follower = Instantiate(gameObject.transform.GetChild(0).gameObject);

        // Destroys it if it exeeds the limited number
        var ok = moleculeManager.AddMolecule(follower);
        if (!ok)
        {
            Destroy(follower);
            StopAllCoroutines();
        }

        // Activate it
        else
        {
            follower.transform.SetParent(gameObject.transform);
            follower.SetActive(true);
        }
    }

    public void StopSpawning()
    {
        continueSpawning = false;
    }
}
