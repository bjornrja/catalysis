﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transition : MonoBehaviour
{

    public float fadeFrequency = 0.05f;
    public float fadeDuration = .5f;

    IEnumerator WaitAndDeactivate(float duration)
    {
        yield return new WaitForSeconds(duration);
        gameObject.SetActive(false);
    }

    public void DelayedDeactivate()
    {
        StartCoroutine(WaitAndDeactivate(fadeDuration));
    }

    public void FadeInSingle()
    {
        StartCoroutine(FadeAndToggleSingle(gameObject, fadeFrequency, fadeDuration, true));
    }
    public void FadeOutSingle()
    {
        StartCoroutine(FadeAndToggleSingle(gameObject, fadeFrequency, fadeDuration, false));
    }

    public void FadeInMaterial(Material material)
    {
        StartCoroutine(FadeMaterial(material, fadeFrequency, fadeDuration, true));
    }
    public void FadeOutMaterial(Material material)
    {
        StartCoroutine(FadeMaterial(material, fadeFrequency, fadeDuration, false));
    }

    private IEnumerator FadeAndToggleSingle(GameObject gObject, float frequency, float duration, bool appear)
    {
        Color color = gObject.GetComponent<Renderer>().material.GetColor("_BaseColor");

        if (!appear)
        {
            for (float newAlpha = 1f; newAlpha >= 0f; newAlpha -= frequency / duration)
            {
                color.a = newAlpha;
                gObject.GetComponent<Renderer>().material.SetColor("_BaseColor", color);
                yield return new WaitForSeconds(frequency);
            }
            gameObject.SetActive(false);
        }

        else
        {
            for (float newAlpha = 0f; newAlpha <= 1f; newAlpha += frequency / duration)
            {
                color.a = newAlpha;
                gObject.GetComponent<Renderer>().material.SetColor("_BaseColor", color);
                yield return new WaitForSeconds(frequency);
            }
            color.a = 1;
            gObject.GetComponent<Renderer>().material.SetColor("_BaseColor", color);



        }
        yield return "Finished";
    }

    private IEnumerator FadeMaterial(Material material, float frequency, float duration, bool appear)
    {
        Color color = material.color;

        if (!appear)
        {
            for (float newAlpha = 1f; newAlpha >= 0f; newAlpha -= frequency / duration)
            {
                color.a = newAlpha;
                material.SetColor("_Basecolor", color);
                material.color = color;
                //Debug.Log(color.a + ", " + material.GetColor("_Basecolor"));
                yield return new WaitForSeconds(frequency);
            }
        }

        else
        {
            for (float newAlpha = 0f; newAlpha <= 1f; newAlpha += frequency / duration)
            {
                color.a = newAlpha;
                material.SetColor("_Basecolor", color);
                yield return new WaitForSeconds(frequency);
            }
        }
        yield return "Finished";
    }

}
