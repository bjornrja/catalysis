﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveIntoScene : MonoBehaviour
{
    // Move GameObject for a period when script is activated. 
    public float speed = 200f;
    public float duration = 1f;
    private Vector3 startVector;
    private float timeElapsed;

    void Start()
    {
        startVector = new Vector3(0, 0, -(speed * duration));
        gameObject.transform.localPosition += startVector;
    }

    void Update()
    {
        timeElapsed += Time.deltaTime;
        if (timeElapsed < duration)
        {
            transform.localPosition = transform.localPosition + (Vector3.forward * speed * Time.deltaTime);
        } 
    }
}
