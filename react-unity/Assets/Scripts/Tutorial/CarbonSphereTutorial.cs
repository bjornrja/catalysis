﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;


public class CarbonSphereTutorial : MonoBehaviour
{
    private TutorialManager tutorialManager;
    private ScenarioManager scenarioManager;
    private float delay;
    private AudioSource audioSource;
    private int timesUsed;

    void Start()
    {
        tutorialManager = GameObject.Find("TutorialManager").GetComponent<TutorialManager>();
        scenarioManager = GameObject.Find("ScenarioManager").GetComponent<ScenarioManager>();
        delay = scenarioManager.transitionDuration;
        audioSource = tutorialManager.gameObject.GetComponent<AudioSource>();

    }
    public void SpherePulled()
    {
        if (timesUsed == 0)
        {
            StartCoroutine(FirstPull(delay));
            gameObject.GetComponent<XRGrabInteractable>().enabled = false;
        }
        else if (timesUsed == 1)
        {
            StartCoroutine(SecondPull(delay));
            gameObject.GetComponent<XRGrabInteractable>().enabled = false;
        }

        timesUsed += 1;
    }

    IEnumerator FirstPull(float seconds)
    {
        audioSource.clip = tutorialManager.clip5;      
        audioSource.Play();

        yield return new WaitForSeconds(seconds);

        tutorialManager.NextTask();

        // temp. method for freeze
        SimpleFreeze(true);
    
        audioSource.clip = tutorialManager.clip6;
        audioSource.Play(); // i fremtiden kan du ikke interagere med objekter. de er låste. 

        yield return new WaitForSeconds(tutorialManager.clip6.length);

        // Activate sphere for backward time-travel
        gameObject.GetComponent<XRGrabInteractable>().enabled = true;
    }

    IEnumerator SecondPull(float seconds)
    {
        yield return new WaitForSeconds(seconds);

        // temp method for unfreeze
        SimpleFreeze(false);


    }



    private void SimpleFreeze(bool enableFreeze)
    {
        tutorialManager.one.GetComponent<XRGrabInteractable>().enabled = !enableFreeze;
        tutorialManager.two.GetComponent<XRGrabInteractable>().enabled = !enableFreeze;
        tutorialManager.three.GetComponent<XRGrabInteractable>().enabled = !enableFreeze;

        if (enableFreeze)
        {

            tutorialManager.one.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
            tutorialManager.two.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
            tutorialManager.three.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;

        }
        else{
            tutorialManager.one.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
            tutorialManager.two.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
            tutorialManager.three.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;

        }
    }
}

