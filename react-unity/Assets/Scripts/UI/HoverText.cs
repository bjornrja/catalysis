﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System;

public class HoverText : MonoBehaviour
{
    // Place a label PreFab above a child GameObject
    public GameObject labelPrefab;
    private GameObject label;
    private Vector3 lookatVector;
    private GameObject childObject;
    public GameObject labelManagerObject;
    public String text;
    public Vector3 positionOffset;

    void Start()
    {
        childObject = gameObject.transform.GetChild(0).gameObject;

        label = Instantiate(labelPrefab);
        var labelText = label.GetComponent<UnityEngine.UI.Text>();

        if (text == "")
        {
            labelText.text = childObject.name;
        }

        else { labelText.text = text; }

        if (positionOffset == new Vector3(0, 0, 0))
        {
            positionOffset = new Vector3(0, 0.1f, 0);
        }

        label.transform.parent = gameObject.transform;
        label.transform.localPosition = new Vector3(0, 0, 0);
        label.GetComponent<Canvas>().worldCamera = Camera.main;
        if (labelManagerObject != null)
        {
            StartCoroutine(AddDelayedListener(.1f));
        }
    }

    private IEnumerator AddDelayedListener(float waitSeconds)
    {
        yield return new WaitForSeconds(waitSeconds);
        if (labelManagerObject.GetComponent<LabelManager>().onLEDGrabbed != null)
        {
            labelManagerObject.GetComponent<LabelManager>().onLEDGrabbed.AddListener(Hide); //må gjhøres generel
        }
    }


    void Update()
    {
        // Make the label face the camera
        label.transform.LookAt(Camera.main.gameObject.transform, lookatVector);

        label.transform.position = childObject.transform.position + positionOffset;

        label.transform.eulerAngles = new Vector3(
            0,
            label.transform.eulerAngles.y + 180,
            0
        );
    }

    public void Hide()
    {
        label.SetActive(false);
    }
}
