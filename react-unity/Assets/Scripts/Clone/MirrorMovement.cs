﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MirrorMovement : MonoBehaviour
{
    public GameObject objectToMirror;
    void Update()
    {
        gameObject.transform.localEulerAngles
         = new Vector3(
             objectToMirror.transform.localEulerAngles.x,
             -objectToMirror.transform.localEulerAngles.y,
             -objectToMirror.transform.localEulerAngles.z);

        gameObject.transform.localPosition
        = new Vector3(
            -objectToMirror.transform.localPosition.x,
            objectToMirror.transform.localPosition.y,
            objectToMirror.transform.localPosition.z);

    }
}
