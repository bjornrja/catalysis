﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToSphere : MonoBehaviour
{
    public GameObject target;

    public float endScale, endTrail, trailShrinkStep, scaleShrinkStep;

    void Update()
    {


        if (Vector3.Distance(gameObject.transform.position, target.transform.position) > .1f)
        {
            gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, target.transform.position, .1f);

            if (gameObject.GetComponent<TrailRenderer>().startWidth > endTrail)
            {
                gameObject.GetComponent<TrailRenderer>().startWidth -= trailShrinkStep;
            }
            if (gameObject.transform.localScale.x > endScale)
            {
                gameObject.transform.localScale -= new Vector3(1, 1, 1) * scaleShrinkStep;
            }

        }

        else if (Vector3.Distance(gameObject.transform.position, target.transform.position) <= .1f)
        {
            gameObject.SetActive(false);
        }

    }
}
