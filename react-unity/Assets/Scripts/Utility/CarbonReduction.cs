﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class CarbonReduction : MonoBehaviour
{
    // Manage molecule removal
    private ScenarioManager scenarioManager;
    private ChalkboardManager chalkboardManager;
    public bool enableChalkboardUpdate = false;
    public string interactionLayer; 

    void Start()
    {
        scenarioManager = GameObject.Find("ScenarioManager").GetComponent<ScenarioManager>();
        if (enableChalkboardUpdate == true)
        {
            chalkboardManager = GameObject.Find("ChalkboardManager").GetComponent<ChalkboardManager>();
        }
    }

    public void RemoveCarbonMolecules(int number)
    {
        if (gameObject.GetComponent<XRBaseInteractor>().selectTarget.gameObject.tag == "Interactable")
        {
            scenarioManager.reduceCO2(number);

            // Update the chalkboard
            if (enableChalkboardUpdate == true)
            {
                chalkboardManager.OnTaskPerformed(interactionLayer);
            }
        }
    }

}
