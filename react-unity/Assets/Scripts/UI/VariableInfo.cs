﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class VariableInfo : MonoBehaviour
{
    // Present variables textually

    private Text infoText;
    private ScenarioManager scenarioManager;
    public enum Variable
    {
        Year,
        SeaLevel,
        Temperature
    }


    public Variable variable;
    void Start()
    {
        scenarioManager = GameObject.Find("ScenarioManager").GetComponent<ScenarioManager>();
        infoText = GetComponent<UnityEngine.UI.Text>();
    }

    void Update()
    {
        if (variable == Variable.Year)
        {
            infoText.text = scenarioManager.currentYear.ToString();
        }

        else if (variable == Variable.SeaLevel)
        {
            infoText.text =  scenarioManager.seaLevelRisen.ToString("N2");
        }
        else if (variable == Variable.Temperature)
        {
            infoText.text = scenarioManager.currentTemperature.ToString("N2");
        }


    }

}
