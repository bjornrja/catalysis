﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;


public class MoleculeBehaviour : MonoBehaviour
{

    [HideInInspector] public float orbitSpeed;
    public Material cutMoleculeMat;
    public AudioClip popSound;

    public float moveSpeed = .1f;
    private Vector3 randomTilt;
    private GameObject parentObject;
    [Header("Measurements in centimeters")]
    public bool moveInOrbit = true;
    public float orbitRadius;
    public float maxTiltOffset;
    private bool changeSide = false;
    [HideInInspector] public bool nextMolecule = false;
    [HideInInspector] public bool following = false;

    public GameObject carbonCutAnchor;
    private GameObject moleculeToFollow;

    void Start()
    {
        // Convert to centimenters
        maxTiltOffset *= 0.01f;
        orbitRadius *= 0.01f;

        orbitSpeed = Random.Range(45f, 100f);

        // Move the molecule to a set position away from the parent and set a random tilt
        if (moveInOrbit)
        {
            parentObject = transform.parent.gameObject;

            transform.localPosition = (parentObject.transform.position + new Vector3(0, 0, 1)).normalized * orbitRadius;

            randomTilt = new Vector3(Random.Range(-maxTiltOffset, maxTiltOffset), Random.Range(-maxTiltOffset, maxTiltOffset), Random.Range(-maxTiltOffset, maxTiltOffset));
        }
    }

    void Update()
    {

        if (moveInOrbit)
        {
            // Orbit the parent
            transform.RotateAround(parentObject.transform.position, randomTilt, orbitSpeed * Time.deltaTime);
        }

        else if (changeSide)
        {
            // Move towards the right hand anchor (carbonCutAnchor)
            float step = moveSpeed * Time.deltaTime;
            transform.SetParent(null);
            transform.position = Vector3.MoveTowards(transform.position, carbonCutAnchor.transform.position, step);
            if (Vector3.Distance(transform.position, carbonCutAnchor.transform.position) < 0.01f)
            {
                changeSide = false;
                carbonCutAnchor.GetComponent<CutCarbon>().AddMolecule(gameObject);
            }
        }

        else if (nextMolecule)
        {
            // Move the molecule to it's parent's position if it is first in line
            if (!gameObject.GetComponent<XRBaseInteractable>().isSelected)
            {
                transform.localPosition = new Vector3(0f, 0f, 0);
            }
        }

        else if (following)
        {
            // Follow the next molecule
            transform.SetParent(null);
            float step = moveSpeed * Time.deltaTime;
            if (Vector3.Distance(transform.position, moleculeToFollow.transform.position) > 0.05f)
            {
                transform.position = Vector3.MoveTowards(transform.position, moleculeToFollow.transform.position, step / 2);
            }
        }
    }

    public void FreeMolecule()
    {
        moveInOrbit = false;
        changeSide = true;
        gameObject.GetComponent<AudioSource>().Play();
        gameObject.GetComponent<Renderer>().material = cutMoleculeMat;
    }

    public void FollowNext(GameObject gObject)
    {
        following = true;
        moleculeToFollow = gObject;
    }
}
