﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoleculeManager : MonoBehaviour
{
    // Defines the amount of followers to spawn
    [SerializeField] public List<GameObject> spawnedFollower;
    public int followerCount = 30;

    private TutorialManager tutorialManager;

    void Start()
    {
        tutorialManager = GameObject.Find("TutorialManager").GetComponent<TutorialManager>();
    }

    public bool AddMolecule(GameObject gameObject)
    {
        if (spawnedFollower.Count < followerCount)
        {
            spawnedFollower.Add(gameObject);
            if (spawnedFollower.Count == followerCount)
            {
                tutorialManager.StartCoroutine(tutorialManager.DelayedNext(5));
            }

            return true;
        }
        else
        {
            return false;
        }
    }
}
