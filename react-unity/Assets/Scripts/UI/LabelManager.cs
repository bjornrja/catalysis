﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR.Interaction.Toolkit;

public class LabelManager : MonoBehaviour
{
    [HideInInspector] public UnityEvent onLEDGrabbed;
    void Start()
    {
        onLEDGrabbed = new UnityEvent();
    }

    public void HideLEDText()
    {
        onLEDGrabbed.Invoke();
    }

}
