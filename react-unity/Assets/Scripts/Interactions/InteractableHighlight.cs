﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class InteractableHighlight : MonoBehaviour
{
    // Changes the color of an interactable object that is hovered by an interactor with the correct layermask. 
    // Use Interactable Events (Hover) in the "XR Grab Interactable" components to invoke StartHighlight and StopHighlight
    public GameObject model;
    public Material highLightMaterial;
    private Material defaultMaterial;
    public LayerMask triggeringLayerMask;
    private XRBaseInteractable baseInteractable;


    void Start()
    {
        defaultMaterial = model.GetComponent<MeshRenderer>().material;
        baseInteractable = gameObject.GetComponent<XRBaseInteractable>();
    }


    public void StartHighlight()
    {
        foreach (XRBaseInteractor hoveringInteractor in baseInteractable.hoveringInteractors)
        {
            // If the hovering interactor equals triggeringLayerMask
            if (hoveringInteractor.interactionLayerMask == (hoveringInteractor.interactionLayerMask | (1 << triggeringLayerMask.value))
            || (hoveringInteractor.interactionLayerMask == triggeringLayerMask.value))
            {


                // Highlight if the object is currently not being selected
                if (!baseInteractable.isSelected)
                {
                    model.GetComponent<MeshRenderer>().material = highLightMaterial;
                }

                // Highlight if the object is currently selected by an environmental interactor(AKA not XR controllers)
                else if (baseInteractable.selectingInteractor.tag == "Interactor")
                {
                    model.GetComponent<MeshRenderer>().material = highLightMaterial;
                }

            }

        }
    }

    public void StopHighlight()
    {
        model.GetComponent<MeshRenderer>().material = defaultMaterial;
    }


}
