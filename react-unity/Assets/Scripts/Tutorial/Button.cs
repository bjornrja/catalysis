﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Button : MonoBehaviour
{
    // Switches between the button's default material and the material that indicates that it's ready to be pushed. 
    public Material readyMat;
    private Material defaultmat;

    void Start()
    {
        defaultmat = gameObject.GetComponent<Renderer>().material;
        SetReady(false);

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Touch Collider")
        {
            onButtonPress.Invoke();
            SetReady(false);
        }
    }

    public UnityEvent onButtonPress;


    public void SetReady(bool ready)
    {
        gameObject.GetComponent<Collider>().enabled = ready;

        if (ready)
        {
            gameObject.GetComponent<Renderer>().material = readyMat;
        }
        else
        {
            gameObject.GetComponent<Renderer>().material = defaultmat;
        }
    }

}
