﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParentAccesser : MonoBehaviour
{
    // Interactors temporarily disconnects their selected Interactable from their parent. 
    // This script is nessecary to access the parent when the XR Interactable is being selected. 
    [HideInInspector] public GameObject parentObject;

    void Start()
    {
        parentObject = gameObject.transform.parent.gameObject;
    }
}
