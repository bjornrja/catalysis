﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;


public class SnapToPosition : MonoBehaviour
{
    // Keeps an Interactable locked to the position of this GameObject when it's not being selected
    public GameObject anchor;
    private XRBaseInteractable interactable;
    void Start()
    {
        interactable = gameObject.GetComponent<XRBaseInteractable>();
    }
    void Update()
    {
        if (interactable.isSelected == false)
        {
            transform.position = anchor.transform.position;
        }
    }
}
