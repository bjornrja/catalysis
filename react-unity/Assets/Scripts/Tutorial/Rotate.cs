﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    public int rotationsPerMinute = 10;
    public Vector3 rotationVector;

    void Update()
    {
        transform.Rotate((rotationVector) * 6.0f * rotationsPerMinute * Time.deltaTime);
    }
}
